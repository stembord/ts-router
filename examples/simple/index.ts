import { Location } from "@stembord/location";
import url = require("url");
import { createContext, IContext, Router } from "../../lib";

const { parse } = url;

export const onLoad = (_: Event): boolean | null => {
  const router = new Router();

  router.use(
    "/",
    (context: IContext) =>
      new Promise((resolve: (context: IContext) => void) => {
        setTimeout(() => {
          console.log("middleware");
          resolve(context);
        }, 0);
      })
  );
  router.route("/", () => {
    console.log("Home");
  });
  router.route("/parent/:parent_id", (context: IContext) => {
    console.log("Parent", context.params);
  });
  router.route("/parent/:parent_id/child/:child_id", (context: IContext) => {
    console.log("Child", context.params);
  });
  router.route(
    "/parent/:parent_id/child/:child_id/grand-child/:grand_child_id",
    (context: IContext) => {
      console.log("Grandchild", context.params);
    }
  );
  router.route("/redirect", (context: IContext) => {
    context.redirectUrl = parse("/");
  });
  router.route("/error", () => {
    throw new Error("rejected");
  });
  router.use("/", (context: IContext) => {
    if (!context.resolved()) {
      context.redirectUrl = parse("/not-found");
    }
    context.end();
  });

  const handler = (url: url.UrlWithParsedQuery) => {
    const context = createContext(url);

    return router.handle(context).then(
      () => {
        if (context.redirectUrl) {
          return Promise.reject(context.redirectUrl);
        } else if (!context.resolved) {
          return Promise.reject(parse("/404"));
        } else {
          return Promise.resolve(context.url);
        }
      },
      error => {
        if (error && context.redirectUrl) {
          return Promise.reject(context.redirectUrl);
        } else if (error) {
          console.error(error);
          return Promise.reject(error);
        } else if (!context.resolved) {
          return Promise.reject(parse("/404"));
        } else {
          return Promise.reject(null);
        }
      }
    );
  };

  const location = new Location(window, { html5Mode: true, handler });

  location.init();

  return null;
};

window.addEventListener("load", onLoad);

if ((module as any).hot) {
  (module as any).hot.accept(() => {
    window.location.reload();
  });
}
