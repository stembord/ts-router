import { none, Option, some } from "@stembord/core";
import { IContext } from "./context";
import { IHandler } from "./IHandler";
import { IMiddleware } from "./IMiddleware";
import { Layer } from "./Layer";
import { Middleware } from "./Middleware";
import { Route } from "./Route";
import { CanceledError, CANCELLED_MESSAGE } from "./utils/CanceledError";
import { promisifyResult } from "./utils/promisifyResult";
import { UnhandledError } from "./utils/UnhandledError";

export class Router extends Layer {
  Middleware: new (
    path?: string,
    parent?: Option<Layer>,
    end?: boolean
  ) => Middleware;
  Route: new (path?: string, parent?: Option<Layer>) => Route;
  Router: new (path?: string, parent?: Option<Layer>) => Router;
  protected layers: Layer[];

  constructor(path: string = "/", parent: Option<Layer> = none()) {
    super(path, parent, false);

    this.Middleware = Middleware;
    this.Route = Route;
    this.Router = Router;

    this.layers = [];
  }

  middleware(error: Error | null, context: IContext): Promise<IContext> {
    const resolveSuccess = (): Promise<IContext> =>
      context.resolved()
        ? Promise.reject(new CanceledError())
        : Promise.resolve(context);

    const resolveError = (error: Error): Promise<IContext> =>
      error && error.message === CANCELLED_MESSAGE
        ? Promise.resolve(context)
        : Promise.reject(error);

    return this.layers
      .reduce(
        (promise, layer) => {
          const params = layer.match(context.pathname);

          if (params) {
            context.params = {
              ...context.params,
              ...params
            };

            if (layer instanceof this.Router) {
              context.route = context.middleware = undefined;
              context.router = layer as Router;
            } else if (layer instanceof this.Route) {
              context.router = context.middleware = undefined;
              context.route = layer as Route;
            } else {
              context.router = context.route = undefined;
              context.middleware = layer as Middleware;
            }
            context.layer = layer;

            return promise
              .then(
                () => promisifyResult(context, layer.middleware(null, context)),
                error =>
                  error && error.message === CANCELLED_MESSAGE
                    ? Promise.reject(error)
                    : promisifyResult(context, layer.middleware(error, context))
              )
              .then(resolveSuccess);
          } else {
            return promise;
          }
        },
        error ? Promise.reject(error) : Promise.resolve(context)
      )
      .then(() => {
        if (!context.resolved()) {
          throw new UnhandledError();
        } else {
          return context;
        }
      }, resolveError);
  }

  handle(context: IContext): Promise<IContext> {
    const params = this.match(context.pathname);

    if (params) {
      context.params = { ...context.params, ...params };
      return this.middleware(null, context);
    } else {
      return Promise.reject(new UnhandledError());
    }
  }

  setPath(path: string): Router {
    super.setPath(path);

    this.layers.forEach(layer => layer.recompilePath());

    return this;
  }

  recompilePath(): Router {
    super.recompilePath();

    this.layers.forEach(layer => layer.recompilePath());

    return this;
  }

  use(
    path: string = "/",
    ...handlers: Array<IHandler | IMiddleware>
  ): Middleware {
    const middleware = new this.Middleware(path, some(this), false).mount(
      ...handlers
    );
    this.layers.push(middleware);
    return middleware;
  }

  route(path: string = "/", ...handlers: Array<IHandler | IMiddleware>): Route {
    const route = new this.Route(path, some(this)).mount(...handlers);
    this.layers.push(route);
    return route;
  }

  scope(path: string = "/"): Router {
    const router = new this.Router(path, some(this));
    this.layers.push(router);
    return router;
  }

  mount(path: string = "/", router: Router): Router {
    this.use(path, router);
    return router;
  }

  clear(): Router {
    this.layers.length = 0;
    return this;
  }
}
