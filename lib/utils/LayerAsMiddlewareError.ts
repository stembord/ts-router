const LAYER_AS_MIDDLEWARE_ERROR_MESSAGE =
  "A Layer should never be called as middleware";

export class LayerAsMiddlewareError extends Error {
  constructor() {
    super(LAYER_AS_MIDDLEWARE_ERROR_MESSAGE);
  }
}
