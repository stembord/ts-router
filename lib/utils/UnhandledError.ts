export const UNHANDLED_MESSAGE = "unhandled";

export class UnhandledError extends Error {
  constructor() {
    super(UNHANDLED_MESSAGE);
  }
}
