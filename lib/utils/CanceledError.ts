export const CANCELLED_MESSAGE = "cancelled";

export class CanceledError extends Error {
  constructor() {
    super(CANCELLED_MESSAGE);
  }
}
