import { Option } from "@stembord/core";
import EventEmitter = require("events");
import { format } from "util";
import { IContext, IParams } from "./context";
import { IMiddleware } from "./IMiddleware";
import { buildPath } from "./utils/buildPath";
import { cleanPath } from "./utils/cleanPath";
import { filterParams } from "./utils/filterParams";
import { LayerAsMiddlewareError } from "./utils/LayerAsMiddlewareError";
import { formatPath, Param, pathToMatcher } from "./utils/pathToMatcher";

export class Layer extends EventEmitter implements IMiddleware {
  protected parent: Option<Layer>;
  protected end: boolean;
  protected relativePath: string;
  protected path: string;
  protected formatPath: string;
  protected params: Param[];
  protected regExp: RegExp;

  constructor(path: string = "/", parent: Option<Layer>, end: boolean = true) {
    super();

    this.parent = parent;
    this.end = !!end;
    this.relativePath = "";
    this.path = "";
    this.formatPath = "";
    this.params = [];
    this.regExp = this.createRegExp(path);
  }

  getParent() {
    return this.parent;
  }
  getRoot(): ThisType<this> {
    return this.getParent()
      .map(parent => parent.getRoot())
      .unwrapOr(this);
  }

  middleware(error: Error | null, context: IContext): Promise<IContext> {
    return Promise.reject(new LayerAsMiddlewareError());
  }

  getFormatPath(): string {
    return this.formatPath;
  }

  getPath(): string {
    return this.path;
  }

  getRegExp(): RegExp {
    return this.regExp;
  }

  isEnd(): boolean {
    return this.end;
  }

  match(path: string): IParams | false {
    return filterParams(this.regExp, this.params, path);
  }

  createRegExp(path: string): RegExp {
    this.relativePath = cleanPath(path);
    this.path = buildPath(this.parent, this.relativePath);
    this.formatPath = formatPath(this.path);
    const { regExp, params } = pathToMatcher(this.path, this.end);
    this.regExp = regExp;
    this.params = params;
    return regExp;
  }

  setPath(path: string): Layer {
    this.createRegExp(path);
    return this;
  }
  recompilePath(): Layer {
    return this.setPath(this.relativePath);
  }

  format(...args: any[]) {
    return format(this.formatPath, ...args);
  }
}
