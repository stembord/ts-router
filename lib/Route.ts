import { none, Option } from "@stembord/core";
import { Layer } from "./Layer";
import { Middleware } from "./Middleware";

export class Route extends Middleware {
  constructor(path: string = "/", parent: Option<Layer> = none()) {
    super(path, parent, true);
  }
}
